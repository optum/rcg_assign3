import React from 'react';

// Router
import {Link} from 'react-router-dom'

import './CourseList.css';

// Data
import CoursesAPI from '../../api_stub'

const CourseList  = () => (

    <div className="CourseList">

        <h1>Amazing Udemy Courses!</h1>
        <h4>By: Maximilian Schwarzmüller</h4>
        <ul>
            {
                CoursesAPI.all().map(course => (
                    <li key={course.id}>
                        <Link to={`/courses/${course.id}`}>{course.title}</Link>
                        <img src={course.image} width='100' height='75' alt={course.alt} />
                        <p>{course.desc}</p>
                    </li>
                ))
            }
        </ul>
    </div>
);

export default CourseList;



//CoursesAPI.all().map( course => {
//    return <article className="Course" key={course.id}>{course.title}</article>;
//} )
