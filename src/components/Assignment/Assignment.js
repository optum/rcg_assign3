/**
 * rcg_assign3_sdavis
 * Created by Steve Davis on 3/29/18.
 * (c) 2018, Steve Davis -- All Rights Reserved.
 */
import React from 'react'

import './Assignment.css'

// This component contains the instruction for the assignment with styling.

const Assignment = () => (
    <div className="Assignment">
        <ol>
            <li><p>Add Routes to load "Users" and "Courses" on different pages (by entering a URL, without Links)</p></li>
            <li><p>Add a simple navigation with two links => One leading to "Users", one leading to "Courses"</p></li>
            <li><p>Make the courses in "Courses" clickable by adding a link and load the "Course" component in the place of "Courses" (without passing any data for now)</p></li>
            <li><p>Pass the course ID to the "Course" page and output it there</p></li>
            <li><p>Pass the course title to the "Course" page - pass it as a param or score bonus points by passing it as query params (you need to manually parse them though!)</p></li>
            <li><p>Load the "Course" component as a nested component of "Courses"</p></li>
            <li><p>Add a 404 error page and render it for any unknown routes</p></li>
            <li><p>Redirect requests to /all-courses to /courses (=> Your "Courses" page)</p></li>
        </ol>

    </div>
);

export default Assignment

