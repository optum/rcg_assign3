/**
 * rcg_assign3_sdavis
 * Created by Steve Davis on 3/29/18.
 * (c) 2018, Steve Davis -- All Rights Reserved.
 */
import React from 'react'

// Router
import { Switch, Route } from 'react-router-dom'

// Components
import CourseList from '../CourseList/CourseList'
import Course from '../Course/Course'

const Courses = () => (
    <Switch>
        <Route exact path='/courses' component={CourseList}/>
        <Route path='/courses/:id' component={Course}/>
    </Switch>
);

export default Courses
