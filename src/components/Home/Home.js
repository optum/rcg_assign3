/**
 * rcg_assign3_sdavis
 * Created by Steve Davis on 3/29/18.
 * (c) 2018, Steve Davis -- All Rights Reserved.
 */
import React from 'react'

// Component
import Assignment from '../Assignment/Assignment';

// This component displays the assignment instructions.

const Home = () => (
    <div>
        <h1>React - the Complete Guide</h1>
        <h3>Assignment 3 -- Routing</h3>
        <Assignment/>
    </div>
);

export default Home