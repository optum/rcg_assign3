import React from 'react'

// Router
import { Link } from 'react-router-dom'

// Query String Parser
import qs from 'query-string';

// Data
import CoursesAPI from '../../api_stub'

import './Course.css';

const Course = (props) => {

    // Get the course for the passed in course number
    const course = CoursesAPI.get(parseInt(props.match.params.id, 10));

    // Get the coupon code from the query parameters
    const couponCode = qs.parse(props.location.search).cc;

    console.log(couponCode);

    if (!course) {
        return <div>So sorry mate.  The course you specified was not found.</div>
    }

    return (
        <div className="Course">
            <h2>{course.title}</h2>
            <p><em>Course ID: {course.id}</em></p>
            <div>
                <table>
                    <tbody>
                    <tr>
                        <td rowSpan="2">
                            <img src={'/' + course.image} alt={course.alt} />
                        </td>
                        <td>
                            <p>{course.desc}</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href={course.extLink} target="_blank">Go to Udemy Course Description</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div>
                    {couponCode ? 'Coupon code (from query parameter cc) = ' + couponCode : 'No Coupon Code Specified.' }
                </div>
            </div>
            <Link to='/courses'>Back to Courses</Link>
        </div>
    )
};

export default Course
