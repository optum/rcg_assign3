/**
 * rcg_assign3_sdavis
 * Created by Steve Davis on 3/29/18.
 * (c) 2018, Steve Davis -- All Rights Reserved.
 */
import React from 'react'

// Router
import { NavLink } from 'react-router-dom'

import './Header.css';

// This Header creates links that can be used to navigate between routes.

const Header = () => (
    <div className="Header">
    <header >
        <nav>
            <ul>
                <li><NavLink to='/' exact activeClassName="my-active">Home</NavLink></li>
                <li><NavLink to='/users' activeClassName="my-active">Users</NavLink></li>
                <li><NavLink to='/courses' activeClassName="my-active">Courses</NavLink></li>
            </ul>
        </nav>
    </header>
    </div>
);

export default Header