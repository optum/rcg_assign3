/**
 * rcg_assign3_sdavis
 * Created by Steve Davis on 3/29/18.
 * (c) 2018, Steve Davis -- All Rights Reserved.
 */
import React from 'react'

// Router
import { Link } from 'react-router-dom'

//import './NotFound.css'

// This component is displayed for 404 errors.

const NotFound = () => (
    <div>
        <h2>NOT FOUND</h2>
        <Link to='/'>Take Me to Safety</Link>
    </div>
);

export default NotFound

