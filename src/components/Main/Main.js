/**
 * rcg_assign3_sdavis
 * Created by Steve Davis on 3/29/18.
 * (c) 2018, Steve Davis -- All Rights Reserved.
 */
import React from 'react'

// Router
import {Switch, Route, Redirect} from 'react-router-dom'

// Components
import Home         from '../Home/Home'
import Users        from '../Users/Users'
import Courses      from '../Courses/Courses'
import NotFound     from '../NotFound/NotFound'

// The Main component renders one of the three provided Routes (provided that one matches):
//      HOME (where the assignment tasks are displayed)
//      Users (a separate "page" for the users.
//      Courses (a list of click-able courses)
// Both the /users and /courses routes will match any pathname that starts with /users or /courses.
// The / route will only match when the pathname is exactly the string "/"

const Main = () => (
    <main>
        <Switch>
            {/* HOME (exact match) -------------------------------------------------------------*/}
            <Route exact path='/' component={Home}/>
            {/* Users --------------------------------------------------------------------------*/}
            <Route path='/users' component={Users}/>
            {/* Courses ------------------------------------------------------------------------*/}
            <Route path='/courses' component={Courses}/>
            {/* REDIRECT: /all-courses to /courses ---------------------------------------------*/}
            <Redirect from="/all-courses" to="/courses"/>
            {/* 404 Error ----------------------------------------------------------------------*/}
            <Route component={NotFound}/>
        </Switch>
    </main>
);

export default Main
