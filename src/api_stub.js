/**
 * rcg_assign3_sdavis
 * Created by Steve Davis on 3/29/18.
 * (c) 2018, Steve Davis -- All Rights Reserved.
 */
//=================================================================================================
// Courses API Stub
//=================================================================================================
//
// This is an API stub for retrieving data about the courses without having to use the component
// state.
//

const CourseAPI = {
    courses: [
        {
            id: 1,
            title: "Angular - The Complete Guide",
            image: "angular.jpg",
            alt: "Angular",
            desc: "Master Angular (Angular 2+, incl. Angular 5) and build awesome, reactive web apps with the successor of Angular.js",
            extLink: "https://www.udemy.com/the-complete-guide-to-angular-2/"
        },
        {
            id: 2,
            title: "Vue - The Complete Guide",
            image: "vue.jpg",
            alt: "Vue",
            desc: "Vue JS is an awesome JavaScript Framework for building Frontend Applications! VueJS mixes the Best of Angular + React!",
            extLink: "https://www.udemy.com/vuejs-2-the-complete-guide/learn/v4/overview"
        },
        {
            id: 3,
            title: "PWA - The Complete Guide",
            image: "pwa.jpg",
            alt: "PWA",
            desc: "Build a Progressive Web App (PWA) that feels like an iOS & Android App, using Device Camera, Push Notifications and more!",
            extLink: "https://www.udemy.com/progressive-web-app-pwa-the-complete-guide/learn/v4/overview"
        },
        {
            id: 4,
            title: "React Native - The Practical Guide",
            image: "rnative.jpg",
            alt: "React Native",
            desc: "Use React Native and your React knowledge and take your web development skills to build native iOS and Android Apps",
            extLink: "https://www.udemy.com/react-native-the-practical-guide/"
        }

    ],

    all: function() { return this.courses},

    get: function(id) {
        const isCourse = c => c.id === id;
        return this.courses.find(isCourse)
    }
};

export default CourseAPI

