import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';

import Header   from './components/Header/Header';
import Main     from './components/Main/Main';

// The main App consists of a navigation header and a main page which contains the main routing
// specification.

class App extends Component {

    render () {

        return (
            <BrowserRouter>
                <div>
                    {/* Navigation Header (with NavLinks) --------------------------------------*/}
                    <Header/>
                    {/* Main Page with Router Specification ------------------------------------*/}
                    <Main/>
                </div>
            </BrowserRouter>
        );

    } // END: render()
}

export default App;
