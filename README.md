##React, the Complete Guide - Udemy Course
###Assignment 3 - Routing Practice
S. Davis - 03/29/2018

#####Added ("Extra Credit") Features:

1.  Used the query-string package to extract a query parameter from the URL.
2.  Added an api stub to retrieve the ccourse information to eliminate the need to store the information in the state.

#####Questions for this Assignment
Q:  What did you find most challenging and how did you overcome the challenge?

A:  My biggest challenge is usually creating and debugging CSS.  My past projects have always used a CSS framework like Bootstrap.  I just need to practice my raw CSS skills!
